document.addEventListener('DOMContentLoaded', () => {
    let regTotal = document.getElementById('regTotal');
    let pieChart = document.getElementById('pieChart');
    let barChart = document.getElementById('barChart');
    fetch('http://localhost:8045/api/chartdata')
        .then(function(response) {
            return response.json()
        })
        .then(function(data) {

            regTotal.innerHTML = data.total.all;
            Chart.defaults.global.legend.position = "bottom";

            getLatestCoupon();
            latestStudent();

            let pChart = new Chart(pieChart, {
                type: 'doughnut',
                data: {
                    labels: ["SMA", "SMP"],
                    datasets: [{
                        label: 'Pendaftar Berdasarkan Sekolah',
                        data: [
                            data.senior.all,
                            data.junior.all,
                        ],
                        backgroundColor: [
                            'rgb(75, 192, 192)',
                            'rgb(255, 205, 86)'
                        ]
                    }],
                },
            });

            let bChart = new Chart(barChart, {
                type: 'bar',
                data: {
                    labels: ["Total", "SMA", "SMP"],
                    datasets: [{
                        label: 'total',
                        backgroundColor: 'rgba(242, 95, 92, 0.5)',
                        borderColor: 'rgba(242, 95, 92, 1)',
                        borderWidth: 1,
                        data: [
                            data.total.all,
                            data.senior.all,
                            data.junior.all
                        ]
                    },{
                        label: 'laki-laki',
                        backgroundColor: 'rgba(80, 81, 79, 0.5)',
                        borderColor: 'rgba(80, 81, 79, 1)',
                        borderWidth: 1,
                        data: [
                            data.total.male,
                            data.senior.male,
                            data.junior.male
                        ]
                    },{
                        label: 'perempuan',
                        backgroundColor: 'rgba(36, 123, 160, 0.5)',
                        borderColor: 'rgba(36, 123, 160, 1)',
                        borderWidth: 1,
                        data: [
                            data.total.female,
                            data.senior.female,
                            data.junior.female
                        ]
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                stepSize: 20,
                            }
                        }],
                    },
                    layout: {
                        padding: {
                            left: 25,
                            right: 25
                        }
                    }
                }
            });
        });
});

const getLatestCoupon = () => {

    let table = document.getElementById('couponTable');

    fetch('http://localhost:8045/api/latecoupon')
        .then(function(response) {
            return response.json()
        })
        .then(function(data) {

            let intTable = '';

            for (let index = 0; index < data.length; index++) {
                const element = data[index];
                
                intTable += "<tr>";
                intTable += "<td>"+ element.coupon +"</td>";
                intTable += "<td class=\"has-text-centered\">";
                intTable += ((element.step1 == 1) ? "<i class=\"icon-ok has-text-success\"></i>" : "<i class=\"icon-cancel has-text-danger\"></i>");
                intTable += "</td>";
                intTable += "<td class=\"has-text-centered\">";
                intTable += ((element.step2 == 1) ? "<i class=\"icon-ok has-text-success\"></i>" : "<i class=\"icon-cancel has-text-danger\"></i>");
                intTable += "</td>";
                intTable += "<td class=\"has-text-centered\">";
                intTable += ((element.step3 == 1) ? "<i class=\"icon-ok has-text-success\"></i>" : "<i class=\"icon-cancel has-text-danger\"></i>") ;
                intTable += "</td>";
                intTable += "</tr>";

                console.log(element.step1);
            }

            table.innerHTML = intTable;
        });
};

const latestStudent = () => {

    let target = document.getElementById('studentTable');

    fetch('http://localhost:8045/api/lateststudent')
        .then(function(response) {
            return response.json()
        })
        .then(function(data) {

            let inTable = '';

            for (let index = 0; index < data.length; index++) {
                const element = data[index];
                
                inTable += "<tr>" + 
                    "<td>" + element.id + "</td>" +
                    "<td>" + element.coupon + "</td>" +
                    "<td>" + ((element.grade === '1') ? "SMP" : "SMA") + "</td>" +
                    "<td>" + element.nama + "</td>" +
                    "<td>" + element.panggilan + "</td>" +
                    "<td>" + (element.gender === 'l' ? 'Laki-laki' : 'Perempuan') + "</td>" +
                    "<td>" + element.anak_ke + "</td>" +
                    "<td>" + element.bersaudara + "</td>" +
                    "<td>" + element.kota + "</td>" +
                    "</tr>";

            }
            target.innerHTML = inTable;
        });
};

let newCoupon = document.getElementById('newCoupon');
let modalClose = document.getElementById('modalClose');
let modal = document.getElementById('couponModal');
let couponOutput = document.getElementById('couponOutput');

newCoupon.addEventListener('click', () => {

	fetch('http://localhost:8045/api/quickcoupon')
		.then(function(response) {
			return response.json()
		})
		.then(function(data) {
            couponOutput.innerHTML = data.coupon;
            getLatestCoupon();
		})
	modal.classList.add('is-active');
});

modalClose.addEventListener('click', () => {
	modal.classList.remove('is-active');
	couponOutput.innerHTML = '';
});