document.addEventListener('DOMContentLoaded', () => {
    let toogler = document.getElementById('type');
    let target1 = document.getElementById('hideable1');
    let target2 = document.getElementById('hideable2');

    
    if (!!toogler) {
        target1.style.display = "none";
        target2.style.display = "none";
        
        toogler.addEventListener('mouseup', () => {
            console.log('toggler exist');
            if (toogler.value == 1 || toogler.value == 2) {
                console.log('executed true');
                target1.style.display = "none";
                target2.style.display = "none";
            } else {
                console.log('executed false');
                target1.removeAttribute('style');
                target2.removeAttribute('style');
            }
        });
    }
});