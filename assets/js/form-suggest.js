let provForm = document.getElementById('prov');
let kotaForm = document.getElementById('kota');


provForm.addEventListener('focus', () => {
    fetch('http://localhost:8045/assets/json/indonesia.json')
        .then(function(response) {
            return response.json()
        })
        .then(function(data) {
            let prov = data["prov"];
            let targetContent = '';
            let target = document.getElementById('provSuggest');

            for (let index = 0; index < prov.length; index++) {
                const element = prov[index];
                
                targetContent += '<option value="'+ element +'">'
            }

            target.innerHTML = targetContent;
        })
});

kotaForm.addEventListener('focus', () => {
    let provValue = provForm.value;
    
    fetch('http://localhost:8045/assets/json/indonesia.json')
        .then(function(response) {
            return response.json()
        })
        .then(function(data) {
            let kota = data.kota[provValue];
            let targetContent = '';
            let target = document.getElementById('kotaSuggest');
            
            for (let index = 0; index < kota.length; index++) {
                const element = kota[index];
                
                targetContent += '<option value="'+ element +'">'
            }

            target.innerHTML = targetContent;
        })
});