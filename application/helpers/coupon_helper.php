<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('generateCoupon'))
{
    function generateCoupon()
    {
        $pool = '123456789abcdefghijklmnopqrstuvwxyz';
        
        $str = '';
        for ($i=0; $i < 5; $i++)
        {
            $str .= substr($pool, mt_rand(0, strlen($pool) -1), 1);
        }
        return $str;
    }   
}