<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('valueTranslate'))
{
    function valueTranslate($type, $value)
    {
        switch ($type) {
            case 'grade':
                switch ($value) {
                    case '1':
                        return 'SMP';
                        break;

                    case '2':
                        return 'SMA';
                        break;
                    
                    default:
                        return 'Invalid Value';
                        break;
                }
                break;

            case 'gender':
                switch ($value) {
                    case 'l':
                        return 'Laki-Laki';
                        break;

                    case 'p':
                        return 'Perempuan';
                        break;
                    
                    default:
                        return 'Invalid Value';
                        break;
                }
                break;

            case 'agama':
                switch ($value) {
                    case '1':
                        return 'Islam';
                        break;

                    case '2':
                        return 'Kriste/Protestan';
                        break;

                    case '3':
                        return 'Katholik';
                        break;

                    case '4':
                        return 'Hindu';
                        break;

                    case '5':
                        return 'Budha';
                        break;

                    case '6':
                        return 'Khong Hu Chu';
                        break;

                    default:
                        return 'Lainnya';
                        break;
                }
                break;
            
            case 'kewarg':
                switch ($value) {
                    case '1':
                        return 'WNI';
                        break;

                    case '2':
                        return 'WNA';
                        break;
                    
                    default:
                        return 'Lainnya';
                        break;
                }
                break;

            case 'relation':
                switch ($value) {
                    case '1':
                        return 'Ayah';
                        break;

                    case '2':
                        return 'Ibu';
                        break;

                    case '3':
                        return 'Kakek';
                        break;

                    case '4':
                        return 'Nenek';
                        break;

                    case '5':
                        return 'Paman';
                        break;

                    case '6':
                        return 'Bibi';
                        break;
                    
                    default:
                        return 'Lainnya';
                        break;
                }
                break;

            case 'yatim':
                switch ($value) {
                    case '1':
                        return 'Yatim';
                        break;

                    case '2':
                        return 'Piatu';
                        break;

                    case '3':
                        return 'Yatim Piatu';
                        break;
                    
                    default:
                        return 'Tidak';
                        break;
                }
                break;

            case 'keb_khusus':
                switch ($value) {
                    case '1':
                        return 'Tidak';
                        break;

                    case '2':
                        return 'Tuna Netra';
                        break;

                    case '3':
                        return 'Tuna Rungu';
                        break;

                    case '4':
                        return 'Tuna Wicara';
                        break;

                    case '5':
                        return 'Tuna Laras';
                        break;

                    case '6':
                        return 'Tuna Ganda';
                        break;

                    case '7':
                        return 'Grahita Ringan';
                        break;

                    case '8':
                        return 'Grahita Sedang';
                        break;

                    case '9':
                        return 'Daksa Ringan';
                        break;

                    case '10':
                        return 'Daksa Sedang';
                        break;

                    case '11':
                        return 'Hyperaktif';
                        break;

                    case '12':
                        return 'Cerdas Istimewa';
                        break;

                    case '13':  
                        return 'Bakat Istimewa';
                        break;

                    case '14':
                        return 'Kesulitan Belajar';
                        break;

                    case '15':
                        return 'Narkoba';
                        break;

                    case '16':
                        return 'Indigo';
                        break;

                    case '17':
                        return 'Down Syndrome';
                        break;

                    case '18':
                        return 'Autis';
                        break;

                    case '19':
                        return 'Terbelakang';
                        break;

                    case '20':
                        return 'Bencana Alam';
                        break;
                    
                    default:
                        return 'Tidak Mampu Ekonomi';
                        break;
                }
                break;

            case 'bln':
                switch ($value) {
                    case '1':
                        return 'Januari';
                        break;

                    case '2':
                        return 'Februari';
                        break;

                    case '3':
                        return 'Maret';
                        break;

                    case '4':
                        return 'April';
                        break;

                    case '5':
                        return 'Mei';
                        break;

                    case '6':
                        return 'Juni';
                        break;

                    case '7':
                        return 'Juli';
                        break;

                    case '8':
                        return 'Agustus';
                        break;

                    case '9':
                        return 'September';
                        break;

                    case '10':
                        return 'Oktober';
                        break;

                    case '11':
                        return 'November';
                        break;

                    case '12':
                        return 'Desember';
                        break;

                    default:
                        return 'Invalid Value';
                        break;
                }
                break;

            case 'pendidikan':
                switch ($value) {
                    case '1':
                        return 'Tidak Sekolah';
                        break;

                    case '2':
                        return 'Putus SD';
                        break;

                    case '3':
                        return 'SD Sederajat';
                        break;

                    case '4':
                        return 'SMP Sederajat';
                        break;

                    case '5':
                        return 'SMA Sederajat';
                        break;

                    case '6':
                        return 'D1';
                        break;

                    case '7':
                        return 'D2';
                        break;

                    case '8':
                        return 'D3';
                        break;

                    case '9':
                        return 'D4/S1';
                        break;

                    case '10':
                        return 'S2';
                        break;

                    case '11':
                        return 'S3';
                        break;

                    default:
                        return 'Invalid Value';
                        break;
                }
                break;

            case 'pekerjaan':
                switch ($value) {
                    case '1':
                        return 'Tidak Bekerja';
                        break;

                    case '2':
                        return 'Nelayan';
                        break;

                    case '3':
                        return 'Petani';
                        break;

                    case '4':
                        return 'Peternak';
                        break;

                    case '5':
                        return 'PNS';
                        break;

                    case '6':
                        return 'Guru / Dosen';
                        break;

                    case '7':
                        return 'TNI / Polri';
                        break;

                    case '8':
                        return 'Karyawan Swasta';
                        break;

                    case '9':
                        return 'Pedagang Kecil';
                        break;

                    case '10':
                        return 'Pedagang Besar';
                        break;

                    case '11':
                        return 'Wiraswasta';
                        break;

                    case '12':
                        return 'Wirausaha';
                        break;

                    case '13':  
                        return 'Buruh';
                        break;

                    case '14':
                        return 'Pensiunan';
                        break;

                    case '15':
                        return 'TKI';
                        break;
                    
                    default:
                        return 'Lainnya';
                        break;
                }
                break;

            case 'penghasilan':
            case 'pengeluaran':
                switch ($value) {
                    case '1':
                        return 'Kurang dari Rp 1.000.000';
                        break;

                    case '2':
                        return 'Rp. 1.000.000 - Rp. 2.000.000';
                        break;
                    
                    default:
                        return 'lebih dari Rp 2.000.000';
                        break;
                }
                break;


            default:
                return 'Invalid type';
                break;
        }
    }   
}