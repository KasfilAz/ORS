<section class="section">
    <div class="container">
        <div class="columns">
            <div class="form-wrapper column is-8 is-offset-2">
                <?= form_open_multipart('entry/data/'.$student_id.'/'.$state) ?>
                <h3 class="is-size-4 has-text-centered is-hidden-mobile">Data <?= ucfirst($state) ?> Calon Santri</h3>
                <?php if ($state == 'wali'): ?>
                <section class="section in-form">
                    <h3 class="form-group-title is-size-5 has-text-centered has-text-weight-semibold has-text-primary">Pilih data wali</h3>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Wali adalah</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control is-expanded">
                                    <div class="select">
                                        <select name="type" id="type">
                                            <option value="1" <?= set_select('type', '1') ?>>Ayah</option>
                                            <option value="2" <?= set_select('type', '2') ?>>Ibu</option>
                                            <option value="3" <?= set_select('type', '3') ?>>Kakek</option>
                                            <option value="4" <?= set_select('type', '4') ?>>Nenek</option>
                                            <option value="5" <?= set_select('type', '5') ?>>Paman</option>
                                            <option value="6" <?= set_select('type', '6') ?>>Bibi</option>
                                            <option value="7" <?= set_select('type', '7') ?>>Lainnya</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <?php endif; ?>
                <section class="section in-form" id="hideable1">
                    <h3 class="form-group-title is-size-5 has-text-centered has-text-weight-semibold has-text-primary">Data diri</h3>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Nama Lengkap</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" name="nama" id="nama" placeholder="Nama Lengkap" maxlength="125" class="input" value="<?= set_value('nama') ?>">
                                </div>
                                <?php if(form_error('nama')): ?>
                                <p class="help is-danger"><?= form_error('nama') ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Tempat</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control is-expanded">
                                    <input type="text" name="tempat_lahir" id="tmpt_lahir" placeholder="Tempat lahir" maxlength="55" class="input" value="<?= set_value('tmpt_lahir') ?>">
                                </div>
                                <?php if(form_error('tmpt_lahir')): ?>
                                <p class="help is-danger"><?= form_error('tmpt_lahir') ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Tanggal Lahir</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="number" name="tgl" id="tgl" placeholder="Tanggal" min="1" max="31" class="input" value="<?= set_value('tgl') ?>">
                                </div>
                                <?php if(form_error('tgl')): ?>
                                <p class="help is-danger"><?= form_error('tgl') ?></p>
                                <?php endif ?>
                            </div>
                            <div class="field">
                                <div class="control is-expanded">
                                    <div class="select">
                                        <select name="bln" id="bln">
                                            <option value="1" <?= set_select('bln', '1') ?>>Januari</option>
                                            <option value="2" <?= set_select('bln', '2') ?>>Februari</option>
                                            <option value="3" <?= set_select('bln', '3') ?>>Maret</option>
                                            <option value="4" <?= set_select('bln', '4') ?>>April</option>
                                            <option value="5" <?= set_select('bln', '5') ?>>Mei</option>
                                            <option value="6" <?= set_select('bln', '6') ?>>Juni</option>
                                            <option value="7" <?= set_select('bln', '7') ?>>Juli</option>
                                            <option value="8" <?= set_select('bln', '8') ?>>Agustus</option>
                                            <option value="9" <?= set_select('bln', '9') ?>>September</option>
                                            <option value="10" <?= set_select('bln', '10') ?>>Oktober</option>
                                            <option value="11" <?= set_select('bln', '11') ?>>November</option>
                                            <option value="12" <?= set_select('bln', '12') ?>>Desember</option>
                                        </select>
                                    </div>
                                </div>
                                <?php if(form_error('bln')): ?>
                                <p class="help is-danger"><?= form_error('bln') ?></p>
                                <?php endif ?>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <input type="number" name="thn" id="thn" placeholder="tahun" min="1880" max="2018" class="input" value="<?= set_value('thn') ?>">
                                </div>
                                <?php if(form_error('thn')): ?>
                                <p class="help is-danger"><?= form_error('thn') ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Kewarganegaraan</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control is-expanded">
                                    <div class="select">
                                        <select name="kewarg" id="kewarg">
                                            <option value="1" <?= set_select('kewarg', '1') ?>>WNI</option>
                                            <option value="2" <?= set_select('kewarg', '2') ?>>WNA</option>
                                            <option value="3" <?= set_select('kewarg', '3') ?>>Lainnya</option>
                                        </select>
                                    </div>
                                </div>
                                <?php if(form_error('kewarg')): ?>
                                <p class="help is-danger"><?= form_error('kewarg') ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Agama</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control is-expanded">
                                    <div class="select">
                                        <select name="agama" id="agama">
                                            <option value="1" <?= set_select('agama', '1') ?>>islam</option>
                                            <option value="2" <?= set_select('agama', '2') ?>>kristen</option>
                                            <option value="3" <?= set_select('agama', '3') ?>>katholik</option>
                                            <option value="4" <?= set_select('agama', '4') ?>>Hindu</option>
                                            <option value="5" <?= set_select('agama', '5') ?>>Budha</option>
                                            <option value="6" <?= set_select('agama', '6') ?>>Khong Hu Chu</option>
                                            <option value="7" <?= set_select('agama', '7') ?>>Lainnya</option>
                                        </select>
                                    </div>
                                </div>
                                <?php if(form_error('agama')): ?>
                                <p class="help is-danger"><?= form_error('agama') ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Status Keluarga</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control is-expanded">
                                    <div class="select">
                                        <select name="masih_bersama" id="masih_bersama">
                                            <option value="1" <?= set_select('masih_bersama', '1') ?>>Masih Bersama</option>
                                            <option value="0" <?= set_select('masih_bersama', '0') ?>>Bercerai</option>
                                        </select>
                                    </div>
                                </div>
                                <?php if(form_error('masih_bersama')): ?>
                                <p class="help is-danger"><?= form_error('masih_bersama') ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Pendidikan</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control is-expanded">
                                    <div class="select">
                                        <select name="pendidikan" id="pendidikan">
                                            <option value="" disabled selected>-- Pilih Jenjang --</option>
                                            <?php for ($i=0; $i < count($pendidikan); $i++):?>
                                            <option value="<?= $i+1 ?>" <?= set_select('pendidikan', $i+1) ?>><?= $pendidikan[$i] ?></option>
                                            <?php endfor ?>
                                        </select>
                                    </div>
                                </div>
                                <?php if(form_error('pendidikan')): ?>
                                <p class="help is-danger"><?= form_error('pendidikan') ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Pekerjaan</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control is-expanded">
                                    <div class="select">
                                        <select name="pekerjaan" id="pekerjaan">
                                            <option value="" disabled selected>-- Pilih pekerjaan --</option>
                                            <?php for ($i=0; $i < count($pekerjaan); $i++):?>
                                            <option value="<?= $i+1 ?>" <?= set_select('pekerjaan', $i+1) ?>><?= $pekerjaan[$i] ?></option>
                                            <?php endfor ?>
                                        </select>
                                    </div>
                                </div>
                                <?php if(form_error('pekerjaan')): ?>
                                <p class="help is-danger"><?= form_error('pekerjaan') ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Penghasilan/bulan</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control is-expanded">
                                    <div class="select">
                                        <select name="penghasilan" id="penghasilan">
                                            <option value="1" <?= set_select('penghasilan', 1) ?>>Kurang dari Rp. 1.000.000</option>
                                            <option value="2" <?= set_select('penghasilan', 2) ?>>Rp. 1.000.000 - Rp. 2.000.000</option>
                                            <option value="3" <?= set_select('penghasilan', 3) ?>>Lebih dari Rp. 2.000.000</option>
                                        </select>
                                    </div>
                                </div>
                                <?php if(form_error('penghasilan')): ?>
                                <p class="help is-danger"><?= form_error('penghasilan') ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Pengeluaran/bulan</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control is-expanded">
                                    <div class="select">
                                        <select name="pengeluaran" id="pengeluaran">
                                            <option value="1" <?= set_select('pengeluaran', 1) ?>>Kurang dari Rp. 1.000.000</option>
                                            <option value="2" <?= set_select('pengeluaran', 2) ?>>Rp. 1.000.000 - Rp. 2.000.000</option>
                                            <option value="3" <?= set_select('pengeluaran', 3) ?>>Lebih dari Rp. 2.000.000</option>
                                        </select>
                                    </div>
                                </div>
                                <?php if(form_error('pengeluaran')): ?>
                                <p class="help is-danger"><?= form_error('pengeluaran') ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Hidup/Meninggal</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control is-expanded">
                                    <div class="select">
                                        <select name="life" id="life">
                                            <option value="1" <?= set_select('life', "1") ?>>Masih Hidup</option>
                                            <option value="0" <?= set_select('life', '0') ?>>Meninggal Dunia</option>
                                        </select>
                                    </div>
                                </div>
                                <?php if(form_error('life')): ?>
                                <p class="help is-danger"><?= form_error('life') ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Nomer Telepon/Hp</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                            <div class="field has-addons">
                                    <div class="control">
                                        <a class="button is-static">
                                            +62
                                        </a>
                                    </div>
                                    <div class="control is-expanded">
                                        <input type="text" name="phone" id="phone" placeholder="Nomer telepon / Hp" maxlength="15" class="input" value="<?= set_value('phone') ?>">
                                    </div>
                                </div>
                                <?php if(form_error('phone')): ?>
                                <p class="help is-danger"><?= form_error('phone') ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="section in-form" id="hideable2">
                    <h3 class="form-group-title is-size-5 has-text-centered has-text-weight-semibold has-text-primary">Upload Berkas</h3>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Pass Foto <?= $state ?></label>
                        </div>
                        <div class="field-body">
                            <div class="file has-name is-fullwidth is-primary">
                                <label class="file-label">
                                    <input class="file-input" id="foto" type="file" name="foto">
                                    <span class="file-cta">
                                        <span class="file-label">
                                            Pilih Berkas
                                        </span>
                                    </span>
                                    <span class="file-name" id="fotoName">
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">KTP <?= $state ?></label>
                        </div>
                        <div class="field-body">
                            <div class="file has-name is-fullwidth is-primary">
                                <label class="file-label">
                                    <input class="file-input" id="ktp" type="file" name="ktp">
                                    <span class="file-cta">
                                        <span class="file-label">
                                            Pilih Berkas
                                        </span>
                                    </span>
                                    <span class="file-name" id="ktpName">
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="section in-form">
                    <div class="field is-grouped is-grouped-right">
                        <p class="control is-expanded has-text-danger">
                            Mohon Periksa Kembali data anda sebelum dikirimkan
                        </p>
                        <p class="control">
                            <input type="submit" class="button is-info" value="Kirimkan">
                        </p>
                    </div>
                </section>
                <?= form_close() ?>
            </div>
        </div>
    </div>
</section>