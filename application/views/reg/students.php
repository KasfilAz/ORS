<section class="section">
    <div class="container">
        <div class="columns">
            <div class="form-wrapper column is-8 is-offset-2">
                <?= form_open_multipart('entry/datasantri?serial='.$coupon) ?>
                <h3 class="is-size-4 has-text-centered is-hidden-mobile">Data Calon Santri</h3>
                <section class="section in-form">
                    <h3 class="form-group-title is-size-5 has-text-centered has-text-weight-semibold has-text-primary">Data diri</h3>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Di terima di</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control is-expanded">
                                    <div class="select">
                                        <select name="grade" id="grade">
                                            <option value="1" <?= set_select('grade', '1') ?>>SMP</option>
                                            <option value="2" <?= set_select('grade', '2') ?>>SMA</option>
                                        </select>
                                    </div>
                                </div>
                                <?php if(form_error('grade')): ?>
                                <p class="help is-danger"><?= form_error('grade') ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Nama Lengkap</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" name="nama" id="nama" placeholder="Nama Lengkap" maxlength="125" class="input" value="<?= set_value('nama') ?>">
                                </div>
                                <?php if(form_error('nama')): ?>
                                <p class="help is-danger"><?= form_error('nama') ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Panggilan</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" name="panggilan" id="panggilan" placeholder="Panggilan" maxlength="55" class="input" value="<?= set_value('panggilan') ?>">
                                </div>
                                <?php if(form_error('panggilan')): ?>
                                <p class="help is-danger"><?= form_error('panggilan') ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Jenis Kelamin</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control is-expanded">
                                    <div class="select">
                                        <select name="gender" id="gender">
                                            <option value="l" <?= set_select('gender', 'l') ?>>Laki - Laki</option>
                                            <option value="p" <?= set_select('gender', 'p') ?>>Perempuan</option>
                                        </select>
                                    </div>
                                </div>
                                <?php if(form_error('gender')): ?>
                                <p class="help is-danger"><?= form_error('gender') ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Tempat</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control is-expanded">
                                    <input type="text" name="tmpt_lahir" id="tmpt_lahir" placeholder="Tempat lahir" maxlength="55" class="input" value="<?= set_value('tmpt_lahir') ?>">
                                </div>
                                <?php if(form_error('tmpt_lahir')): ?>
                                <p class="help is-danger"><?= form_error('tmpt_lahir') ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Tanggal Lahir</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="number" name="tgl" id="tgl" placeholder="Tanggal" min="1" max="31" class="input" value="<?= set_value('tgl') ?>">
                                </div>
                                <?php if(form_error('tgl')): ?>
                                <p class="help is-danger"><?= form_error('tgl') ?></p>
                                <?php endif ?>
                            </div>
                            <div class="field">
                                <div class="control is-expanded">
                                    <div class="select">
                                        <select name="bln" id="bln">
                                            <option value="1" <?= set_select('bln', '1') ?>>Januari</option>
                                            <option value="2" <?= set_select('bln', '2') ?>>Februari</option>
                                            <option value="3" <?= set_select('bln', '3') ?>>Maret</option>
                                            <option value="4" <?= set_select('bln', '4') ?>>April</option>
                                            <option value="5" <?= set_select('bln', '5') ?>>Mei</option>
                                            <option value="6" <?= set_select('bln', '6') ?>>Juni</option>
                                            <option value="7" <?= set_select('bln', '7') ?>>Juli</option>
                                            <option value="8" <?= set_select('bln', '8') ?>>Agustus</option>
                                            <option value="9" <?= set_select('bln', '9') ?>>September</option>
                                            <option value="10" <?= set_select('bln', '10') ?>>Oktober</option>
                                            <option value="11" <?= set_select('bln', '11') ?>>November</option>
                                            <option value="12" <?= set_select('bln', '12') ?>>Desember</option>
                                        </select>
                                    </div>
                                </div>
                                <?php if(form_error('bln')): ?>
                                <p class="help is-danger"><?= form_error('bln') ?></p>
                                <?php endif ?>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <input type="number" name="thn" id="thn" placeholder="tahun" min="1990" max="2018" class="input" value="<?= set_value('thn') ?>">
                                </div>
                                <?php if(form_error('thn')): ?>
                                <p class="help is-danger"><?= form_error('thn') ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Kewarganegaraan</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control is-expanded">
                                    <div class="select">
                                        <select name="kewarg" id="kewarg">
                                            <option value="1" <?= set_select('kewarg', '1') ?>>WNI</option>
                                            <option value="2" <?= set_select('kewarg', '2') ?>>WNA</option>
                                            <option value="3" <?= set_select('kewarg', '3') ?>>Lainnya</option>
                                        </select>
                                    </div>
                                </div>
                                <?php if(form_error('kewarg')): ?>
                                <p class="help is-danger"><?= form_error('kewarg') ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Agama</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control is-expanded">
                                    <div class="select">
                                        <select name="agama" id="agama">
                                            <option value="1" <?= set_select('agama', '1') ?>>islam</option>
                                            <option value="2" <?= set_select('agama', '2') ?>>kristen</option>
                                            <option value="3" <?= set_select('agama', '3') ?>>katholik</option>
                                            <option value="4" <?= set_select('agama', '4') ?>>Hindu</option>
                                            <option value="5" <?= set_select('agama', '5') ?>>Budha</option>
                                            <option value="6" <?= set_select('agama', '6') ?>>Khong Hu Chu</option>
                                            <option value="7" <?= set_select('agama', '7') ?>>Lainnya</option>
                                        </select>
                                    </div>
                                </div>
                                <?php if(form_error('agama')): ?>
                                <p class="help is-danger"><?= form_error('agama') ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Anak ke</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="number" name="anak_ke" id="anak_ke" placeholder="Anak ke" min="1" max="99" class="input" value="<?= set_value('anak_ke') ?>">
                                </div>
                                <?php if(form_error('anak_ke')): ?>
                                <p class="help is-danger"><?= form_error('anak_ke') ?></p>
                                <?php endif ?>
                            </div>
                            <div class="field">
                                <div class="field has-addons">
                                    <div class="control">
                                        <a class="button is-static">
                                            dari
                                        </a>
                                    </div>
                                    <div class="control">
                                        <input type="number" name="bersaudara" id="bersaudara" placeholder="" min="1" max="99" class="input" value="<?= set_value('bersaudara') ?>">
                                    </div>
                                    <div class="control">
                                        <a class="button is-static">
                                            bersaudara
                                        </a>
                                    </div>
                                </div>
                                <?php if(form_error('bersaudara')): ?>
                                <p class="help is-danger"><?= form_error('bersaudara') ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label"></label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="field has-addons">
                                    <div class="control">
                                        <a class="button is-static">
                                            Saudara tiri
                                        </a>
                                    </div>
                                    <div class="control">
                                        <input type="number" name="tiri" id="tiri" value="0" min="0" max="99" class="input" value="<?= set_value('tiri') ?>">
                                    </div>
                                </div>
                                <?php if(form_error('tiri')): ?>
                                <p class="help is-danger"><?= form_error('tiri') ?></p>
                                <?php endif ?>
                            </div>
                            <div class="field">
                                <div class="field has-addons">
                                    <div class="control">
                                        <a class="button is-static">
                                            Saudara angkat
                                        </a>
                                    </div>
                                    <div class="control">
                                        <input type="number" name="angkat" id="angkat" value="0" min="0" max="99" class="input" value="<?= set_value('angkat') ?>">
                                    </div>
                                </div>
                                <?php if(form_error('angkat')): ?>
                                <p class="help is-danger"><?= form_error('angkat') ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Status yatim</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control is-expanded">
                                    <div class="select">
                                        <select name="yatim" id="yatim">
                                            <option value="0" <?= set_select('yatim', '0') ?>>Tidak</option>
                                            <option value="1" <?= set_select('yatim', '1') ?>>Yatim</option>
                                            <option value="2" <?= set_select('yatim', '2') ?>>Piatu</option>
                                            <option value="3" <?= set_select('yatim', '3') ?>>Yatim Piatu</option>
                                        </select>
                                    </div>
                                </div>
                                <?php if(form_error('yatim')): ?>
                                <p class="help is-danger"><?= form_error('yatim') ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">NISN</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" name="nisn" id="nisn" placeholder="NISN" maxlength="10" class="input" value="<?= set_value('nisn') ?>">
                                </div>
                                <?php if(form_error('nisn')): ?>
                                <p class="help is-danger"><?= form_error('nisn') ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">NIK</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" name="nik" id="nik" placeholder="NIK" maxlength="16" class="input" value="<?= set_value('nik') ?>">
                                </div>
                                <?php if(form_error('nik')): ?>
                                <p class="help is-danger"><?= form_error('nik') ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="section in-form">
                    <h3 class="form-group-title is-size-5 has-text-centered has-text-weight-semibold has-text-primary">Data Kesehatan</h3>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Umum</label>
                        </div>
                        <div class="field-body">
                            <div class="field is-expanded">
                                <div class="field has-addons">
                                    <div class="control">
                                        <a class="button is-static">
                                            Tinggi
                                        </a>
                                    </div>
                                    <div class="control is-expanded">
                                        <input type="number" name="tinggi" id="tinggi" placeholder="cm" min="45" max="250" class="input" value="<?= set_value('tinggi') ?>">
                                    </div>
                                </div>
                                <?php if(form_error('tinggi')): ?>
                                <p class="help is-danger"><?= form_error('tinggi') ?></p>
                                <?php endif ?>
                            </div>
                            <div class="field is-expanded">
                                <div class="field has-addons">
                                    <div class="control">
                                        <a class="button is-static">
                                            Berat
                                        </a>
                                    </div>
                                    <div class="control is-expanded">
                                        <input type="number" name="berat" id="berat" placeholder="Kg" min="15" max="200" class="input" value="<?= set_value('berat') ?>">
                                    </div>
                                </div>
                                <?php if(form_error('berat')): ?>
                                <p class="help is-danger"><?= form_error('berat') ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label"></label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="field has-addons">
                                    <div class="control">
                                        <a class="button is-static">
                                            Golongan Darah
                                        </a>
                                    </div>
                                    <div class="control is-expanded">
                                        <div class="select">
                                            <select name="gol_darah" id="gol_darah">
                                                <option value="a" <?= set_select('gol_darah', 'a') ?>>A</option>
                                                <option value="b" <?= set_select('gol_darah', 'b') ?>>B</option>
                                                <option value="ab" <?= set_select('gol_darah', 'ab') ?>>AB</option>
                                                <option value="o" <?= set_select('gol_darah', 'o') ?>>O</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <?php if(form_error('gol_darah')): ?>
                                <p class="help is-danger"><?= form_error('gol_darah') ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Kebutuhan Khusus</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control is-expanded">
                                    <div class="select">
                                        <select name="keb_khusus" id="keb_khusus">
                                            <?php for ($i=0; $i < count($kebKhusus); $i++):?>
                                            <option value="<?= $i+1 ?>" <?= set_select('gol_darah', $i+1) ?>><?= $kebKhusus[$i] ?></option>
                                            <?php endfor ?>
                                        </select>
                                    </div>
                                </div>
                                <?php if(form_error('keb_khusus')): ?>
                                <p class="help is-danger"><?= form_error('keb_khusus') ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="section in-form">
                    <h3 class="form-group-title is-size-5 has-text-centered has-text-weight-semibold has-text-primary">Data Tempat Tinggal</h3>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Provinsi</label>
                        </div>
                        <div class="field-body">
                            <div class="field is-expanded">
                                <div class="control">
                                    <input type="text" name="prov" id="prov" placeholder="Provinsi" list="provSuggest" min="45" max="250" class="input" value="<?= set_value('prov') ?>">
                                    <datalist id="provSuggest"></datalist>
                                </div>
                                <?php if(form_error('prov')): ?>
                                <p class="help is-danger"><?= form_error('prov') ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Kota/Kabupaten</label>
                        </div>
                        <div class="field-body">
                            <div class="field is-expanded">
                                <div class="control">
                                    <input type="text" name="kota" id="kota" placeholder="Kota/Kabupaten" list="kotaSuggest" min="45" max="250" class="input" value="<?= set_value('kota') ?>">
                                    <datalist id="kotaSuggest"></datalist>
                                </div>
                                <?php if(form_error('kota')): ?>
                                <p class="help is-danger"><?= form_error('kota') ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Kecamatan</label>
                        </div>
                        <div class="field-body">
                            <div class="field is-expanded">
                                <div class="control">
                                    <input type="text" name="kecamatan" id="kecamatan" placeholder="Kecamatan" min="45" max="250" class="input" value="<?= set_value('kecamatan') ?>">
                                </div>
                                <?php if(form_error('kecamatan')): ?>
                                <p class="help is-danger"><?= form_error('kecamatan') ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Desa</label>
                        </div>
                        <div class="field-body">
                            <div class="field is-expanded">
                                <div class="control">
                                    <input type="text" name="desa" id="desa" placeholder="Desa" min="45" max="250" class="input" value="<?= set_value('desa') ?>">
                                </div>
                                <?php if(form_error('desa')): ?>
                                <p class="help is-danger"><?= form_error('desa') ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Alamat</label>
                        </div>
                        <div class="field-body">
                            <div class="field is-expanded">
                                <div class="control">
                                    <input type="text" name="alamat" id="alamat" placeholder="Alamat" min="45" max="250" class="input" value="<?= set_value('alamat') ?>">
                                </div>
                                <?php if(form_error('alamat')): ?>
                                <p class="help is-danger"><?= form_error('alamat') ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label"></label>
                        </div>
                        <div class="field-body">
                            <div class="field is-expanded">
                                <div class="field has-addons">
                                    <div class="control">
                                        <a class="button is-static">
                                            RT
                                        </a>
                                    </div>
                                    <div class="control is-expanded">
                                        <input type="number" name="rt" id="rt" min="1" max="99" class="input" value="<?= set_value('rt') ?>">
                                    </div>
                                </div>
                                <?php if(form_error('rt')): ?>
                                <p class="help is-danger"><?= form_error('rt') ?></p>
                                <?php endif ?>
                            </div>
                            <div class="field is-expanded">
                                <div class="field has-addons">
                                    <div class="control">
                                        <a class="button is-static">
                                            RW
                                        </a>
                                    </div>
                                    <div class="control is-expanded">
                                        <input type="number" name="rw" id="rw" min="1" max="99" class="input" value="<?= set_value('rw') ?>">
                                    </div>
                                </div>
                                <?php if(form_error('rw')): ?>
                                <p class="help is-danger"><?= form_error('rw') ?></p>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="section in-form">
                    <h3 class="form-group-title is-size-5 has-text-centered has-text-weight-semibold has-text-primary">Upload Berkas (bisa menyusul)</h3>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Ijasah</label>
                        </div>
                        <div class="field-body">
                            <div class="file has-name is-fullwidth is-primary">
                                <label class="file-label">
                                    <input class="file-input" id="ijasah" type="file" name="ijasah">
                                    <span class="file-cta">
                                        <span class="file-label">
                                            Pilih Berkas
                                        </span>
                                    </span>
                                    <span class="file-name" id="ijasahName">
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">SKHU</label>
                        </div>
                        <div class="field-body">
                            <div class="file has-name is-fullwidth is-primary">
                                <label class="file-label">
                                    <input class="file-input" id="skhu" type="file" name="skhu">
                                    <span class="file-cta">
                                        <span class="file-label">
                                            Pilih Berkas
                                        </span>
                                    </span>
                                    <span class="file-name" id="skhuName">
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Akta Kelahiran</label>
                        </div>
                        <div class="field-body">
                            <div class="file has-name is-fullwidth is-primary">
                                <label class="file-label">
                                    <input class="file-input" id="akta" type="file" name="akta">
                                    <span class="file-cta">
                                        <span class="file-label">
                                            Pilih Berkas
                                        </span>
                                    </span>
                                    <span class="file-name" id="aktaName">
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Kartu Keluarga</label>
                        </div>
                        <div class="field-body">
                            <div class="file has-name is-fullwidth is-primary">
                                <label class="file-label">
                                    <input class="file-input" id="kk" type="file" name="kk">
                                    <span class="file-cta">
                                        <span class="file-label">
                                            Pilih Berkas
                                        </span>
                                    </span>
                                    <span class="file-name" id="kkName">
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="section in-form">
                    <div class="field is-grouped is-grouped-right">
                        <p class="control is-expanded has-text-danger">
                            Mohon Periksa Kembali data anda sebelum dikirimkan
                        </p>
                        <p class="control">
                            <input type="submit" class="button is-info" value="Kirimkan">
                        </p>
                    </div>
                </section>
                <?= form_close() ?>
            </div>
        </div>
    </div>
</section>