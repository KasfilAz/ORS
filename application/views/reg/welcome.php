<section class="hero base-gradient">
	<div class="hero-body">
		<div class="columns">
			<div class="column is-6 is-offset-3 has-background-white">
				<h3 class="is-size-4 has-text-centered has-text-weight-bold">Selamat Datang</h3>
				<p class="is-size-5 has-text-centered has-text-primary">Di Pendaftaran Online Calon Santri</p>
				<p class="is-size-5 has-text-centered has-text-weight-semibold">Pondok Pesantren Jawaahirul Hikmah</p>
				<section class="section in-form">
					<h3 class="form-group-title is-size-5 has-text-centered has-text-weight-semibold has-text-primary">Syarat & Ketentuan Pendaftaran</h3>
					<div class="content">
						<ol type="1">
							<li>Meminta Keode pendaftaran kepada nomer dibawah ini:
                                <ul>
                                    <li>Bahrur Rohim : 0852-5717-6316</li>
                                    <li>Kasfil Aziz : 0821-4100-3177</li>
                                </ul>
                                </br>
                            </li>
							<li>Telah lulus SD / MI Sederajat bagi yang akan melanjutkan ke SMP, dan telah lulus SMP Sederajat bagi yang akan melanjutkan ke SMA</li>
							<li>Diantarkan Orang Tua saat interview (Kecuali Yatim Piatu)</li>
							<li>Mau bertempat tinggal diasrama yang telah disediakan</li>
							<li>Mengisi formulir online yang telah disediakan.</li>
							<li>Mengunggah / meng-Upload scan Fotocopy Ijazah yang telah dilegalisir <em>(bisa menyusul jika belum ada)</em>.</li>
							<li>Mengunggah / meng-Upload scan Fotocopy SKHU yang telah dilegalisir <em>(bisa menyusul jika belum ada)</em></li>
							<li>Mengunggah / meng-Upload scan Fotocopy KK (Kartu Keluarga).</li>
							<li>Mengunggah / meng-Upload scan Fotocopy Akte Kelahiran.</li>
							<li>Mengunggah / meng-Upload scan Pass Foto Orang Tua Ukuran 3 x 4.</li>
							<li>Mengunggah / meng-Upload scan Fotocopy KTP Orang Tua yang masih berlaku.</li>
						</ol>
					</div>
				</section>
				<section class="section has-text-centered">
					<?php if(isset($message)): ?>
					<div class="notification is-danger">
						<?= $message ?>
					</div>
					<?php endif; ?>
					<?= form_open('/') ?>
					<div class="field has-addons">
						<div class="control is-expanded">
							<input name="coupon" type="text" class="input" value="<?= set_value('coupon') ?>" placeholder="Kode Pendaftaran">
						</div>
						<div class="control">
							<input type="submit" value="Mulai Daftar" class="button is-info">
						</div>
					</div>
					<?php if(form_error('coupon')): ?>
					<p class="help is-danger">
						<?= form_error('coupon') ?>
					</p>
					<?php endif; ?>
					<?= form_close() ?>
				</section>
			</div>
		</div>
	</div>
</section>
