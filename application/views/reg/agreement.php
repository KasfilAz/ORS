<section class="section">
    <div class="container">
        <div class="columns">
            <div class="form-wrapper column is-8 is-offset-2">
                <?= form_open('entry/data/'.$student_id.'/agreement') ?>
                <h3 class="is-size-4 has-text-centered is-hidden-mobile">Pernyataan & Persetujuan</h3>
                <p class="is-size-5 has-text-centered has-text-danger">Mohon dibaca dengan teliti & seksama</p>
                <section class="section in-form">
                    <h3 class="form-group-title is-size-5 has-text-centered has-text-weight-semibold has-text-primary">Pernyataan Santri</h3>
                    <div class="content">
                        <p>Dengan sungguh-sungguh dan penuh kesadaran.</p>
                        <p class="has-text-centered has-text-weight-semibold">MENYATAKAN</p>
                        <p>Bahwa selama menjadi santri, saya:</p>
                        <ol type="1">
                            <li>Sanggup belajar dengan tekun dan penuh semangat.</li>
                            <li>Sanggup menjaga nama baik diri sendiri, keluarga, sekolah, dan pondoknya</li>
                            <li>Sanggup mentaati dan mematuhi ketentuan tata tertib sekolah dan tata tertib Pondok Pesantren Jawaahirul Hikmah.</li>
                            <li>Apabila tidak mentaati ketentuan yang diterapkan oleh Pondok Pesantren Jawaahirul Hikmah, saya sanggup menerima sanksi, yaitu:
                                <ol type="a">
                                    <li>Hukuman sesuai yang diterapkan oleh Pondok Pesantren Jawaahirul Hikmah.</li>
                                    <li>Dikembalikan ke orang tua (dikeluarkn).</li>
                                </ol>
                            </li>
                            <li>Sanggup menjalankan maklumat, yaitu melaksanakan Pendidikan di Pondok Pesantren Jawaahirul Hikmah selama 6 tahun dan ditambah 1 tahun masa pengabdian dan pendalaman agama. Dan bila daftar mulai jenjang Sekolah Menengah Atas (SMA), melaksanakan pendidikan selama 3 tahun ditambah 1 tahun masa pengabdian dan pendalaman agama.</li>
                        </ol>
                        <p>Pernyataan ini saya buat dengan sebenar-benarnya dan dengan penuh rasa tanggung jawab serta diketahui Orang Tua.</p>
                    </div>
                </section>
                <section class="section in-form">
                    <h3 class="form-group-title is-size-5 has-text-centered has-text-weight-semibold has-text-primary">Pernyataan Orang Tua Santri</h3>
                    <div class="content">
                        <p>Dengan sungguh-sungguh dan penuh kesadaran.</p>
                        <p class="has-text-centered has-text-weight-semibold">MENYATAKAN</p>
                        <p>Bahwa selama menjadi wali santri, saya:</p>
                        <ol type="1">
                            <li>Sanggup mentaati peraturan dan tata tertib pondok.</li>
                            <li>Sanggup menjaga nama baik sekolah dan pondoknya.</li>
                            <li>Sanggup memenuhi segala kebutuhan santri / anak saya selama masih belajar di Pondok Pesantren Jawaahirul Hikmah.</li>
                            <li>Apabila anak saya <strong>terlibat Pencurian</strong> dan / atau <strong>Berpacaran</strong> maka anak saya siap menerima sanksi sebagai berikut:
                                <ol type="a">
                                    <li>1x pelanggaran akan disiarkan (diarak) keliling pondok dan mengembalikan barang yang dicuri.</li>
                                    <li>2x pelanggaran akan dikeluarkan dan mengembalikan barang yang dicuri.</li>
                                </ol>
                            </li>
                        </ol>
                        <p>Pertanyaan ini saya buat dengan sebenar-benarnya dan dengan penuh rasa tanggung jawab</p>
                    </div>
                </section>
                <section class="section in-form">
                    <label class="checkbox">
                        <input type="checkbox" name="pernyataan" value="1">
                            Saya telah faham, setuju dan mengerti <?= form_error('pernyataan') ? "<span class=\"has-text-danger\">anda harus setuju dan mengerti sebelum melanjutkan</span>" : '' ?>
                    </label>
                </section>
                <section class="section in-form">
                    <div class="field is-grouped is-grouped-centered">
                        <p class="control">
                            <input type="submit" class="button is-info" value="selanjutnya">
                        </p>
                    </div>
                </section>
                <?= form_close() ?>
            </div>
        </div>
    </div>
</section>