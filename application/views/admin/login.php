<section class="hero is-light is-bold is-fullheight">
  <div class="hero-body">
    <div class="container has-text-centered">
      <h1 class="title">
        Login as Admin
      </h1>
      <?= form_open('/admin/login') ?>
      <div class="form-login-box">
        <div class="field">
          <div class="control has-icons-left">
            <input class="input" type="text" placeholder="Username" name="username" autofocus="true" value="<?= set_value('username') ?>">
            <span class="icon is-small is-left">
              <i class="icon-user"></i>
            </span>
          </div>
          <?php if(form_error('username')): ?>
          <p class="help is-danger"><?= form_error('username') ?></p>
          <?php endif; ?>
        </div>
        <div class="field">
          <div class="control has-icons-left">
            <input class="input" type="password" placeholder="Password" name="password">
            <span class="icon is-small is-left">
              <i class="icon-lock-filled"></i>
            </span>
          </div>
          <?php if(form_error('password')): ?>
          <p class="help is-danger"><?= form_error('password') ?></p>
          <?php endif; ?>
        </div>
        <div class="field is-grouped">
          <div class="control is-expanded">
            <a href="#" class="button is-text">Forget password?</a>
          </div>
          <div class="control">
            <input type="submit" value="Login" class="button is-info wider">
          </div>
        </div>
        <?php if(isset($message)): ?>
        <div class="notification is-danger">
          <?= $message ?>
        </div>
        <?php endif; ?>
      </div>
      <?= form_close() ?>
    </div>
  </div>
</section>