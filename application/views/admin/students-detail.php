<?php $walis = ['ayah', 'ibu', 'wali'] ?>

<section class="section">
    <div class="container">
        <div class="columns">
            <div class="form-wrapper column is-8 is-offset-2">
                <h3 class="is-size-4 has-text-centered is-hidden-mobile">Detail Data Calon Santri</h3>
                <section class="section in-form" id="hideable1">
                    <h3 class="form-group-title is-size-5 has-text-centered has-text-weight-semibold has-text-primary">Data Calon Santri</h3>
                    <div class="field is-horizontal ">
                        <div class="field-label is-normal">
                            <label class="label">ID</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input readonly" value="<?= $student['id'] ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal ">
                        <div class="field-label is-normal">
                            <label class="label">Diterima di</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input readonly" value="<?= valueTranslate('grade', $student['grade']) ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal ">
                        <div class="field-label is-normal">
                            <label class="label">Nama Lengkap</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input readonly" value="<?= ucwords($student['nama']) ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal ">
                        <div class="field-label is-normal">
                            <label class="label">Nama Panggilan</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input readonly" value="<?= ucwords($student['panggilan']) ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal ">
                        <div class="field-label is-normal">
                            <label class="label">Jenis Kelamin</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input readonly" value="<?= valueTranslate('gender', $student['gender']) ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal ">
                        <div class="field-label is-normal">
                            <label class="label">Tempat Tanggal Lahir</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input readonly" value="<?= ucfirst($student['tempat_lahir']) .', '.$student['tgl'].' '.valueTranslate('bln', $student['bln']).' '.$student['thn'] ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal ">
                        <div class="field-label is-normal">
                            <label class="label">NISN</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input readonly" value="<?= $student['nisn'] ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal ">
                        <div class="field-label is-normal">
                            <label class="label">NIK</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input readonly" value="<?= $student['nik'] ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal ">
                        <div class="field-label is-normal">
                            <label class="label">Kewarganegaraan</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input readonly" value="<?= valueTranslate('kewarg', $student['kewarg']) ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal ">
                        <div class="field-label is-normal">
                            <label class="label">Agama</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input readonly" value="<?= valueTranslate('agama', $student['agama']) ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal ">
                        <div class="field-label is-normal">
                            <label class="label">Anak ke</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input readonly" value="<?=  $student['anak_ke'].' dari '.$student['bersaudara'].' bersaudara' ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal ">
                        <div class="field-label is-normal">
                            <label class="label"></label>
                        </div>
                        <div class="field-body">
                            <div class="field is-expanded">
                                <div class="field has-addons">
                                    <div class="control is-expanded">
                                        <input type="text" class="input readonly" value="<?= $student['tiri'] ?>" readonly>
                                    </div>
                                    <div class="control">
                                        <a class="button is-static">
                                            Saudara Tiri
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="field is-expanded">
                                <div class="field has-addons">
                                    <div class="control is-expanded">
                                        <input type="text" class="input readonly" value="<?= $student['angkat'] ?>" readonly>
                                    </div>
                                    <div class="control">
                                        <a class="button is-static">
                                            Saudara Angkat
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal ">
                        <div class="field-label is-normal">
                            <label class="label">Status Yatim</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input readonly" value="<?= valueTranslate('yatim', $student['yatim']) ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal ">
                        <div class="field-label is-normal">
                            <label class="label">Golongan Darah</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input readonly" value="<?= strtoupper($student['gol_darah']) ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal ">
                        <div class="field-label is-normal">
                            <label class="label">Tinggi</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input readonly" value="<?= $student['tinggi'].' cm' ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal ">
                        <div class="field-label is-normal">
                            <label class="label">Berat</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input readonly" value="<?= $student['berat'].' Kg' ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal ">
                        <div class="field-label is-normal">
                            <label class="label">Kebutuhan Khusus</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input readonly" value="<?= valueTranslate('keb_khusus', $student['keb_khusus']) ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal ">
                        <div class="field-label is-normal">
                            <label class="label">Alamat</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input readonly" value="<?= $student['alamat'] ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label"></label>
                        </div>
                        <div class="field-body">
                            <div class="field is-expanded">
                                <div class="field has-addons">
                                    <div class="control">
                                        <a class="button is-static">
                                            RT
                                        </a>
                                    </div>
                                    <div class="control is-expanded">
                                        <input type="text" class="input readonly" value="<?= $student['rt'] ?>" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="field is-expanded">
                                <div class="field has-addons">
                                    <div class="control">
                                        <a class="button is-static">
                                            RW
                                        </a>
                                    </div>
                                    <div class="control is-expanded">
                                        <input type="text" class="input readonly" value="<?= $student['rw'] ?>" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal ">
                        <div class="field-label is-normal">
                            <label class="label">Desa</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input readonly" value="<?= ucwords($student['desa']) ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal ">
                        <div class="field-label is-normal">
                            <label class="label">Kecamatan</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input readonly" value="<?= ucwords($student['kecamatan']) ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal ">
                        <div class="field-label is-normal">
                            <label class="label">Kabupaten / Kota</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input readonly" value="<?= ucwords($student['kota']) ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal ">
                        <div class="field-label is-normal">
                            <label class="label">Provinsi</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input readonly" value="<?= ucwords($student['prov']) ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <?php for ($i=0; $i < count($walis); $i++): ?>
                <section class="section in-form" id="hideable2">
                    <h3 class="form-group-title is-size-5 has-text-centered has-text-weight-semibold has-text-primary">Data <?= $walis[$i] ?></h3>
                    <div class="field is-horizontal ">
                        <div class="field-label is-normal">
                            <label class="label">Nama</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input readonly" value="<?= ucwords($student[$walis[$i].'Nama']) ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if ($walis[$i] == 'wali'): ?>
                    <div class="field is-horizontal ">
                        <div class="field-label is-normal">
                            <label class="label">Hubungan dengan santri</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input readonly" value="<?= valueTranslate('relation', $student[$walis[$i].'Type']) ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endif ?>
                    <div class="field is-horizontal ">
                        <div class="field-label is-normal">
                            <label class="label">Tempat Tanggal Lahir</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input readonly" value="<?= ucfirst($student[$walis[$i].'TempatLahir']) .', '.$student[$walis[$i].'Tgl'].' '.valueTranslate('bln', $student[$walis[$i].'Bln']).' '.$student[$walis[$i].'Thn'] ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal ">
                        <div class="field-label is-normal">
                            <label class="label">Agama</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input readonly" value="<?= valueTranslate('agama', $student[$walis[$i].'Agama']) ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal ">
                        <div class="field-label is-normal">
                            <label class="label">Kewarganegaraan</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input readonly" value="<?= valueTranslate('kewarg', $student[$walis[$i].'Kewarg']) ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal ">
                        <div class="field-label is-normal">
                            <label class="label">Status Perkawinan</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input readonly" value="<?= $student[$walis[$i].'MasihBersama'] == 1 ? 'Masih Bersama' : 'Cerai' ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal ">
                        <div class="field-label is-normal">
                            <label class="label">Pendidikan</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input readonly" value="<?= valueTranslate('pendidikan', $student[$walis[$i].'Pendidikan']) ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal ">
                        <div class="field-label is-normal">
                            <label class="label">Pekerjaan</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input readonly" value="<?= valueTranslate('pekerjaan', $student[$walis[$i].'Pekerjaan']) ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal ">
                        <div class="field-label is-normal">
                            <label class="label">Penghasilan/Bulan</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input readonly" value="<?= valueTranslate('penghasilan', $student[$walis[$i].'Penghasilan']) ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal ">
                        <div class="field-label is-normal">
                            <label class="label">Pengeluaran/Bulan</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input readonly" value="<?= valueTranslate('pengeluaran', $student[$walis[$i].'Pengeluaran']) ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal ">
                        <div class="field-label is-normal">
                            <label class="label">Hidup / Meninggal</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input readonly" value="<?= $student[$walis[$i].'Life'] == 1 ? 'Masih Hidup' : 'Meninggal Dunia' ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal ">
                        <div class="field-label is-normal">
                            <label class="label">Nomer Tlpon</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input type="text" class="input readonly" value="+62 <?= $student[$walis[$i].'Phone'] ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <?php endfor ?>
            </div>
        </div>
    </div>
</section>