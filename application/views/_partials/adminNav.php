    <nav class="navbar is-dark" role="navigation">
        <div class="navbar-brand">
            <a class="navbar-item" href="<?= route('Admin.Home') ?>">
                <strong>Pendaftaran Santri Baru 2019</strong>
            </a>
            <a role="button" class="navbar-burger" data-target="collapseNav" aria-label="menu" aria-expanded="false">
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
            </a>
        </div>

        <div class="navbar-menu" id="collapseNav">
            <div class="navbar-start">
                <a href="<?= route('Admin.DataEntry') ?>" class="navbar-item <?= $title === 'Data Santri Masuk' ? 'is-active' : '' ?>">Data Santri Masuk</a>
            </div>
            <div class="navbar-end">
                <?php if($this->ion_auth->is_admin()): ?>
                <a href="<?=base_url().'admin/users'?>" class="navbar-item">Users</a>
                <?php endif ?>
                <a href="<?=base_url().'admin/profile'?>" class="navbar-item">Profil (<?=$this->ion_auth->user()->row()->username?>)</a>
                <a href="<?= route('Admin.Logout') ?>" class="navbar-item logout">Logout</a>
            </div>
        </div>
    </nav>