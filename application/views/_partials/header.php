<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url().'assets/img/favicon-16x16.png' ?>">
    <title><?= $title ?></title>

    <!-- echo all css if it is set -->
    <?php if(isset($css)): ?>
    <?php foreach ($css as $file): ?>
    <?= css($file)."\n" ?>
    <?php endforeach; ?>
    <?php endif; ?>

    <!-- echo all js file if it is set -->
    <?php if(isset($headJS)): ?>
    <?php foreach ($headJS as $js): ?>
    <?= js($js)."\n" ?>
    <?php endforeach; ?>
    <?php endif; ?>
</head>
<body>