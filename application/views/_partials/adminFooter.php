<footer class="footer">
  <div class="content has-text-centered">
    <p>
      <strong>Pendaftaran Online Santri Baru <?= date("m") >= 7 ? date("Y") + 1 : date("Y") ?></strong></br>
      <em>Copyright</em> &#169; Jawaahirul Hikmah Islamic Boarding School
    </p>
  </div>
</footer>