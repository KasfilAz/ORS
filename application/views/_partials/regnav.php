<nav class="navbar is-primary" role="navigation" aria-label="main navigation">
	<div class="navbar-brand has-text-centered">
		<p class="navbar-item has-text-weight-semibold">
			Pendaftaran Santri Baru <?= date("m") >= 7 ? date("Y") + 1 : date("Y") ?>
		</p>
	</div>
</nav>
