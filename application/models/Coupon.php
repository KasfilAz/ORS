<?php

class Coupon extends CI_Model {
    public $coupon;
    public $complete_at;

    /**
     * Check the coupon existance from
     * the coupon insert
     *
     * @param string $coupon
     * @return object
     */
    public function existCheck($coupon) {
        $this->db->from('coupons');
        $this->db->where('coupon', $coupon);
        $this->db->limit(1);
        $query = $this->db->get();

        return $query->row();
    }

    /**
     * Update step for coupon's owner
     * by the step approached
     *
     * @param int $id
     * @param string $step
     * @return void
     */
    public function updateStep($id,$step) {
        
        if ($step == 'step3') {
            $this->db->set('complete_at', date("Y-m-d H:i:s"));
        }

        $this->db->set($step, 1);
        $this->db->where('id', $id);
        $this->db->update('coupons');
    }
}