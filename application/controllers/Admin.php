<?php 

class Admin extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->helper(['form', 'translate']);
        $this->load->library(['form_validation']);
        $this->load->model(['Students']);

    }

    public function index() {
        // load page common needs
        $data['title'] = 'Admin Home';
        $data['css'] = [
            'bulma/bulma.min.css',
            'fontello/css/fontello.css',
            'admin/admin.css'
        ];
        $data['headJS'] = [
            'Chart.bundle.min.js',
        ];
        $data['footJS'] = [
            'interaction.js',
            'admin.js'
        ];

        // load all desired pages
        $this->load->view('_partials/header.php', $data);
        $this->load->view('_partials/adminNav.php', $data);
        $this->load->view('admin/index.php', $data);
        $this->load->view('_partials/adminFooter.php');
        $this->load->view('_partials/footer.php', $data);
    }

    public function santriData() {
        
        $data = [
            'title' => 'Data Santri Masuk',
            'css' => [
                'bulma/bulma.min.css',
                'fontello/css/fontello.css',
                'admin/admin.css'
            ],
            'studentTotal' => $this->db->count_all('students'),
            'students' => $this->Students->getAllCompleteStudents()
        ];

        $this->load->view('_partials/header.php', $data);
        $this->load->view('_partials/adminNav.php', $data);
        $this->load->view('admin/students-complete.php', $data);
        $this->load->view('_partials/adminFooter.php');
        $this->load->view('_partials/footer.php', $data);

        // header('Content-type: application/json');
        // echo json_encode($students);
    }

    public function rawSantriData($pageNum = 1) {

        $this->load->library('pagination');
        $limit = 15;

        $students = $this->Students->getRawStudents($limit, $pageNum);

        $config = [
            'base_url' => base_url().'admin/data/raw/santri',
            'total_rows' => $students['total_count'],
            'per_page' => $limit,
            'use_page_numbers' => TRUE,
            'full_tag_open' => '<ul class="pagination-list">',
            'full_tag_close' => '</ul>',
            'num_tag_open' => '<li>',
            'num_tag_close' => '</li>',
            'cur_tag_open' => '<li><p class="pagination-link is-current">',
            'cur_tag_close' => '</p></li>',
            'next_link' => '<i class="icon-right-open"></i>',
            'prev_link' => '<i class="icon-left-open"></i>',
            'first_link' => '<i class="icon-left-open"></i><i class="icon-left-open"></i>',
            'last_link' => '<i class="icon-right-open"></i><i class="icon-right-open"></i>',
            'attributes' => ['class' => 'pagination-link']
        ];

        $this->pagination->initialize($config);

        $data = [
            'title' => 'Data Raw Santri Masuk',
            'css' => [
                'bulma/bulma.min.css',
                'fontello/css/fontello.css',
                'admin/admin.css'
            ],
            'students' => $students['row']
        ];

        
        $data['paginate'] = $this->pagination->create_links();

        $this->load->view('_partials/header.php', $data);
        $this->load->view('_partials/adminNav.php', $data);
        $this->load->view('admin/students-raw.php', $data);
        $this->load->view('_partials/adminFooter.php');
        $this->load->view('_partials/footer.php', $data);
    }

    public function studentDetail($id) {

        $student = $this->Students->getStudentWithId($id);

        $data = [
            'title' => 'Detail Data Santri',
            'css' => [
                'bulma/bulma.min.css',
                'fontello/css/fontello.css',
                'reg.css',
                'admin/admin.css'
            ],
            'student' => $student
        ];

        $this->load->view('_partials/header.php', $data);
        $this->load->view('_partials/adminNav.php', $data);
        $this->load->view('admin/students-detail.php', $data);
        $this->load->view('_partials/adminFooter.php');
        $this->load->view('_partials/footer.php', $data);
    }

    public function users() {

        $data = [
            'title' => 'Users',
            'css' => [
                'bulma/bulma.min.css',
                'fontello/css/fontello.css',
                'reg.css',
                'admin/admin.css'
            ],
            'footJS' => [
                'add-user-modal.js'
            ],
            'users' => $this->ion_auth->users()->result_array(),
            'groups' => $this->ion_auth->groups()->result_array()
        ];

        $this->load->view('_partials/header.php', $data);
        $this->load->view('_partials/adminNav.php', $data);
        $this->load->view('admin/users.php', $data);
        $this->load->view('_partials/adminFooter.php');
        $this->load->view('_partials/footer.php', $data);
    }

    public function profile() {
        $user = $this->ion_auth->user()->row_array();

        $data = [
            'title' => 'Users',
            'css' => [
                'bulma/bulma.min.css',
                'fontello/css/fontello.css',
                'reg.css',
                'admin/admin.css'
            ],
            'id' => $user['id'],
            'username' => $user['username'],
            'email' => $user['email']
        ];

        $this->load->view('_partials/header.php', $data);
        $this->load->view('_partials/adminNav.php', $data);
        $this->load->view('admin/user-profile.php', $data);
        $this->load->view('_partials/adminFooter.php');
        $this->load->view('_partials/footer.php', $data);
    }
}