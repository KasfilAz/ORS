<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(['form']);
        $this->load->library(['form_validation']);
    }

    public function login() {
        // get all user input
        $identifier = $this->input->post('username');
        $password = $this->input->post('password');
        $remember = FALSE;

        $rules = [
            [
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required',
                'errors' => [
                    'required' => 'username harus diisi'
                ]
            ],
            [
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required',
                'errors' => [
                    'required' => 'password harus diisi'
                ]
            ]
        ];

        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() === FALSE) {
            // load common page need
            $data['title'] = 'Admin Login';
            $data['css'] = [
                'bulma/bulma.min.css',
                'fontello/css/fontello.css',
                'admin/admin.css'
            ];

            // get flash message if there is one
            if ($this->session->flashdata('message')) {
                $data['message'] = $this->session->flashdata('message');
            }

            $this->load->view('_partials/header.php', $data);
            $this->load->view('admin/login.php', $data);
            $this->load->view('_partials/footer.php', $data);
        } else {
            // login user from their input
            // if user input is invalid
            if (!$this->ion_auth->login($identifier, $password, $remember)) {
                // set flash message for user information
                // then redirect to admin login page
                $this->session->set_flashdata('message', 'Invalid identity, please try again.');
                redirect('admin/login', 'refresh');
            } else {
                // set next page if the user is will be redirected
                $nextPage = $this->input->get('return');
                // if url contain return in GET method redirect there
                if ($nextPage) {
                    redirect(urldecode($nextPage), 'refresh');
                } else {
                    redirect('admin/', 'refresh');
                }
            }
        }
    }

    public function addUser() {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $email = $this->input->post('email');
        $groups = $this->input->post('groups');

        var_dump($group);

        $rules = [
            [
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required|is_unique[users.username]',
            ],
            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'valid_email'
            ],
            [
                'field' => 'password',
                'lbael' => 'Password',
                'rules' => 'required|min_length[6]'
            ]
        ];

        $this->form_validation->set_rules($rules);

        if($this->form_validation->run() === TRUE) {
            $this->ion_auth->register($username, $password, $email, NULL, $groups);
        }
        redirect(base_url().'admin/users', 'refresh');
    }

    public function updateProfile($id) {
        $currentUser = $this->ion_auth->user()->row()->id == $id;

        $username = $this->input->post('username');
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        if ($currentUser) {
            if ($username != '') {
                $data['username'] = $username;
            }

            if ($password != '') {
                $data['password'] = $password;
            }

            $data['email'] = $email;

            $this->ion_auth->update($id, $data);
            
            if (isset($data['password'])) {
                redirect(route('Admin.Logout'), 'refresh');
            }
            
            redirect(route('Admin.Profile'), 'refresh');
        } else {
            redirect(route('Admin.Home'), 'refresh');
        }
    }

    public function deactivate($id) {
        $data['active'] = 0;
        $this->ion_auth->update($id, $data);
        redirect(base_url().'admin/users', 'refresh');
    }

    public function activate($id) {
        $data['active'] = 1;
        $this->ion_auth->update($id, $data);
        redirect(base_url().'admin/users', 'refresh');
    }

    // logging out current user
    public function logout() {
        $this->ion_auth->logout();
        redirect('admin/login', 'refresh');
    }
}